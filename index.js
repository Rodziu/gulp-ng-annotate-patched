"use strict";

const through = require("through2"),
	ngAnnotate = require("ng-annotate-patched"),
	applySourceMap = require("vinyl-sourcemaps-apply"),
	merge = require("merge"),
	BufferStreams = require("bufferstreams"),
	PluginError = require("plugin-error"),
	pluginName = "gulp-ng-annotate-patched";

// Function which handle logic for both stream and buffer modes.
function transform(file, input, opts) {
	const res = ngAnnotate(input.toString(), opts);
	if (res.errors) {
		let filename = "";
		if (file.path) {
			filename = file.relative + ": ";
		}
		throw new PluginError(pluginName, filename + res.errors.join("\n"));
	}

	if (opts.map && file.sourceMap) {
		var sourceMap = JSON.parse(res.map);
		sourceMap.file = file.relative;
		applySourceMap(file, sourceMap);
	}

	return new Buffer(res.src);
}

module.exports = function (options) {
	options = options || {};
	if (!options.remove) {
		options = merge({add: true}, options);
	};

	return through.obj(function (file, enc, done) {
		// When null just pass through.
		if (file.isNull()) {
			this.push(file);
			return done();
		}

		var opts = merge({map: !!file.sourceMap}, options);
		if (opts.map) {
			if (typeof opts.map === "boolean") {
				opts.map = {};
			}
			if (file.path) {
				opts.map.inFile = file.relative;
			}
		}

		// Buffer input.
		if (file.isBuffer()) {
			try {
				file.contents = transform(file, file.contents, opts);
			} catch (e) {
				this.emit("error", e);
				return done();
			}
			// Dealing with stream input.
		} else {
			file.contents = file.contents.pipe(new BufferStreams(function(err, buf, cb) {
				if (err) return cb(new PluginError(pluginName, err));
				try {
					var transformed = transform(file, buf, opts)
				} catch (e) {
					return cb(e);
				}
				cb(null, transformed);
			}));
		}

		this.push(file);
		done();
	});
};
